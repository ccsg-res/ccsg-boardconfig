/*******************************************************************************
 * Board Configuration tools
 *
 * Copyright (c) 2019,2020 by Bob Iannucci
 *******************************************************************************/

#ifndef __CCSG_BOARD_CONFIG_H__
#define __CCSG_BOARD_CONFIG_H__

#include "variant.h"

#define CCSG_BSP_LIBRARY_VERSION "2020.0621.1"


// WARNING: when adding a new board type, it is imperative to
// go through the project source files as well as the CCSG
// library source files to add the new type as appropriate
#if defined(CMU_BOARD_TYPE)
	#if CMU_BOARD_TYPE == ENVISENSE20
		#define BOARD_ENVISENSE20
		#define BOARD_ENVISENSE_FAMILY
	#elif CMU_BOARD_TYPE == ENVISENSE22
		#define BOARD_ENVISENSE22
		#define BOARD_ENVISENSE_FAMILY
	#elif CMU_BOARD_TYPE == ENVISENSE23
		#define BOARD_ENVISENSE23
		#define BOARD_ENVISENSE_FAMILY
	#elif CMU_BOARD_TYPE == ENVISENSE24
		#define BOARD_ENVISENSE24
		#define BOARD_ENVISENSE_FAMILY
	#elif CMU_BOARD_TYPE == POWERDUE
		#define BOARD_POWERDUE
	#endif
#elif defined(_VARIANT_ARDUINO_ZERO_)
	#define BOARD_FEATHER
#endif

#if defined(BOARD_ENVISENSE_FAMILY)
	// Libraries that need to be enabled
	//   DueFlashStorage
	// Libraries that need to be disabled
	//   RTCZero
	#define DEBUG_SERIAL Serial
	#define DEBUG_BAUD 230400
	#define INCLUDE_DEBUG_FUNCTIONS
	#define INCLUDE_FILE_FUNCTIONS
	#define LED_OFF HIGH
	#define LED_ON LOW
	#define LED_RED             12
	#define LED_GREEN           11
	#define SYSLOG_DIRECTORY "LOG_DIR"
	#define SYSLOG_FILE_PATH "LOG.TXT"
	#define SYSLOG_BACKUP_PATTERN "LOG%05d.TXT"
	#define DATALOG_FILE_PATH "sensdata.jsn"
	#define TEST_DIRECTORY "test_dir"
	#define TEST_FILE_NAME "test.txt"
#elif defined(BOARD_FEATHER)
	// Libraries that need to be enabled
	//   RTCZero
	// Libraries that need to be disabled
	//   DueFlashStorage
	#include <USB/USBAPI.h>
	#define DEBUG_SERIAL SERIAL_PORT_USBVIRTUAL
	#define DEBUG_BAUD 0
	#define INCLUDE_DEBUG_FUNCTIONS
	//#define INCLUDE_FILE_FUNCTIONS
	#define LED_OFF HIGH
	#define LED_ON LOW
	#define LED_RED             0
	#define LED_GREEN           0
#endif


#endif  // __CCSG_BOARD_CONFIG_H__
